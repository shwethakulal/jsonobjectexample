﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace JsonObjectExample
{
    class RestApiAccessProgram
    {

        static void Main(string[] args)
        {
            RunAsync().Wait();
            Console.ReadKey();
        }
        static async Task RunAsync()
        {

            using (var client = new HttpClient())
            {


                HttpResponseMessage response = await client.GetAsync("https://reqres.in/api/users/");

                response.EnsureSuccessStatusCode();
                using (HttpContent content = response.Content)
                {
                    List<User> user = new List<User>();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    JavaScriptSerializer ser = new JavaScriptSerializer();

                    //List<User> users = ser.Deserialize<List<User>>(responseBody);
                    Response usersResponse = JsonConvert.DeserializeObject<Response>(responseBody);
                    Console.WriteLine("Page:{0}", usersResponse.page);
                    Console.WriteLine("Per Page:{0}", usersResponse.per_page);
                    Console.WriteLine("Total:{0}", usersResponse.total);
                    Console.WriteLine("Total pages:{0}", usersResponse.total_pages);
                    foreach (User users in usersResponse.data)
                    {
                        Console.WriteLine("");
                        Console.WriteLine("User Id:{0}", users.id);
                        Console.WriteLine("First name:{0}", users.first_name);
                        Console.WriteLine("Last name:{0}", users.last_name);
                        Console.WriteLine("avatar:{0}", users.avatar);
                    }
                    // Console.WriteLine(responseBody);

                }


            }
        }

    }
}
